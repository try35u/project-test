@include('layout.header')
<?php 
use App\Http\Controllers\PembelianController;
?>
<div class="row">
  <div class="col-sm-5">
    <h1>Detail Pembelian #{{$id}}</h1>
    <table class="table">
        <thead>
          
        <tr>
          <th style="width: 20%">Barang</th>
          <th style="width: 10%">Jumlah</th>
          <th style="width: 20%">Harga Satuan</th>
        </tr>
        </thead>
        <tbody>
          
        @foreach ($laporan as $value)
        <tr>
          <th><?php echo PembelianController::getBarangByID($value->master_barang_id)?></th>
          <th>{{ $value->jumlah }}</th>
         <th align="right" style="text-align: right;">{{ number_format($value->harga_satuan) }}</th>
        </tr>
        @endforeach
        </tbody>

    </table>
  </div>
</div>
@include('layout.footer')