@include('layout.header')
<?php 
// print_r($laporan);
?>
<div class="row">
  <div class="col-sm-12">
    <h1>Laporan Pembelian</h1>
    <table class="table">
        <thead>
          
        <tr>
          <th style="width: 10%">ID Transaksi</th>
          <th style="width: 20%">Tanggal Transaksi</th>
          <th style="width: 20%">Total Transaksi</th>
          <th>Aksi</th>
        </tr>
        </thead>
        <tbody>
          
        @foreach ($laporan as $value)
        <tr>
          <th>{{ $value->id }}</th>
          <th>{{ $value->created_at }}</th>
          <th align="right" style="text-align: right;">{{ number_format($value->total_harga) }}</th>
          <th><a href="<?php echo action("PembelianController@laporan_detail",['id'=>$value->id]) ?>" class="btn btn-primary">Lihat Detail</a></th>
        </tr>
        @endforeach
        </tbody>

    </table>
  </div>
</div>
@include('layout.footer')