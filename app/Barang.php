<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public $table ='master_barang';

      public function detail()
    {
        return $this->hasMany('App\PembelianBarang');
    }
}
