<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\Barang;
use App\Pembelian;
use App\PembelianBarang;
use Illuminate\Http\Request;

// use \App\Http\Requests\StoreBlogPost;

// date_default_timezone_set("Asia/Jakarta");
class PembelianController extends Controller {

	public function pembelian(){
		$Barang = Barang::all();
		// echo view('layout.header');
		echo view('pembelian',['Barang'=>$Barang]);
		// echo view('layout.header');


	}

	public function laporan(){
		$Barang = Barang::all();
		$laporan = Pembelian::all();
	    // echo view('layout.header',['Barang'=>$Barang]);
	    echo view('laporan',[
	    	'Barang'=>$Barang,
	    	'laporan'=>$laporan,
	    ]);
	    // echo view('layout.footer',['Barang'=>$Barang]);
	}
	public static function getBarangByID($id){
		$Barang = Barang::find($id);
		return $Barang->nama_barang;
	}
	public function laporan_detail(Request $r){
		// echo $r->id;
		// exit;
		$laporan = PembelianBarang::where("transaksi_pembelian_id",$r->id)->get();
		// echo "<pre>";
		// print_r($laporan);
		// exit;
	    // echo view('layout.header',['Barang'=>$Barang]);
	    echo view('laporan_detail',[
	    	'id'=>$r->id,
	    	'laporan'=>$laporan,
	    ]);
	    // echo view('layout.footer',['Barang'=>$Barang]);
	}
	public function check(){
		$id = $_REQUEST['id'];
		$model = Barang::find($id);
		return json_encode($model);
	}

	public function simpan(Request $r){

		if (isset($r)){
			$Pembelian = new Pembelian;
			$Pembelian->total_harga = $r->head['total'];
			$Pembelian->created_at = date("Y-m-d H:i:s");
			if ($Pembelian->save()){
				foreach ($r->jsonObj as $key => $value) {
					$PembelianBarang = new PembelianBarang;
					$PembelianBarang->transaksi_pembelian_id = $Pembelian->id;
					$PembelianBarang->master_barang_id = $value['data_id'];
					$PembelianBarang->jumlah = $value['kuantitas'];
					$PembelianBarang->harga_satuan = $value['harga'];
					$PembelianBarang->save();
				}
				echo json_encode(['success'=>true,'message'=>"Transaksi berhasil disimpan"]);
			}

		}else{
			echo json_encode(['success'=>false,'message'=>"Terjadi kesalahan, Silahkan coba lagi "]);
		}

	}

}