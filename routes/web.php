<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App/Barang;

// Route::get('/', function () {
// 	$Barang = Barang::all();
//     return view('welcome');
// });




Route::get('/','PembelianController@pembelian', function () {});
Route::get('/check','PembelianController@check', function () {});
Route::get('/laporan','PembelianController@laporan', function () {});
Route::get('/laporan_detail','PembelianController@laporan_detail', function () {});
Route::get('/simpan','PembelianController@simpan', function () {});
